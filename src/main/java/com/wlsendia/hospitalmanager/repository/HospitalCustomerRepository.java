package com.wlsendia.hospitalmanager.repository;

import com.wlsendia.hospitalmanager.entity.HospitalCustomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HospitalCustomerRepository extends JpaRepository<HospitalCustomer, Long> {
}
