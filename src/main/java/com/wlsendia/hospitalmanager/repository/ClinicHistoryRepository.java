package com.wlsendia.hospitalmanager.repository;

import com.wlsendia.hospitalmanager.entity.ClinicHistory;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ClinicHistoryRepository extends JpaRepository<ClinicHistory, Long> {
    List<ClinicHistory> findAllByDateCureOrderByIdDesc(LocalDate dateCure);
    List<ClinicHistory> findAllByIsSalaryAndDateCureAndIsCalculateOrderById(
            Boolean isSalary, LocalDate dateCure, Boolean isCalculate);

    ClinicHistory findAllById(Long id);
}