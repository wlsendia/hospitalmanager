package com.wlsendia.hospitalmanager.controller;

import com.wlsendia.hospitalmanager.entity.HospitalCustomer;
import com.wlsendia.hospitalmanager.model.CustomerRequest;
import com.wlsendia.hospitalmanager.service.CustomerService;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/customer")
public class CustomerController {
    private final CustomerService customerService;

    @PostMapping("/join")
    public String setCustomer(@RequestBody @Valid CustomerRequest request) {
        customerService.setCustomer(request);

        return request.getCustomerName() + " 등록";
    }

    @GetMapping("/customerId/{customerId}")
    public HospitalCustomer getData(@PathVariable Long customerId) {
        return customerService.getData(customerId);
    }
}