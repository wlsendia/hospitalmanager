package com.wlsendia.hospitalmanager.controller;

import com.wlsendia.hospitalmanager.entity.HospitalCustomer;
import com.wlsendia.hospitalmanager.model.HistoryInsuranceCorporationItem;
import com.wlsendia.hospitalmanager.model.HistoryItem;
import com.wlsendia.hospitalmanager.model.HistoryRequest;
import com.wlsendia.hospitalmanager.model.HistoryResponse;
import com.wlsendia.hospitalmanager.service.CustomerService;
import com.wlsendia.hospitalmanager.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {
    private final CustomerService customerService;
    private final HistoryService historyService;

    @PostMapping("/new/customer-id/{customerId}")
    public String setHistory(@PathVariable long customerId, @RequestBody @Valid HistoryRequest request) {
        HospitalCustomer hospitalCustomer = customerService.getData(customerId);
        historyService.setHistory(hospitalCustomer, request);

        return "OK";
    }

    @GetMapping("/all/date")
    public List<HistoryItem> getHistoriesByDate(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return historyService.getHistoriesByDate(searchDate);

    }

    @GetMapping("/all/insurance")
    public List<HistoryInsuranceCorporationItem> getNeedBillCorporation(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return historyService.getNeedBillCorporation(searchDate);
    }

    @GetMapping("/id/{id}")
    public HistoryResponse getHistory(@PathVariable Long id) {
        return historyService.getHistory(id);
    }

}
