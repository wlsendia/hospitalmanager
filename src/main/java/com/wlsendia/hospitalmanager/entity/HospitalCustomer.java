package com.wlsendia.hospitalmanager.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Getter
@Setter
@Entity
public class HospitalCustomer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String customerName;

    @Column(nullable = false, length = 13)
    private String customerPhone;

    @Column(nullable = false, length = 14)
    private String registrationNumber;

    @Column(nullable = false, length = 100)
    private String address;

    @Column(nullable = false, columnDefinition = "TEXT")
    private String memo;

    @Column(nullable = false)
    private LocalDate dateCreate;

}
