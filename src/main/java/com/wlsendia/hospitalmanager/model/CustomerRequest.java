package com.wlsendia.hospitalmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {

    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;

    @NotNull
    @Length(min = 13, max = 13)
    private String customerPhone;

    @NotNull
    @Length(min = 9, max = 14)
    private String registrationNumber;

    @NotNull
    @Length(min = 5, max = 100)
    private String address;

    @NotNull
    @Length(min = 2)
    private String memo;

}
