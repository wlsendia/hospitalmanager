package com.wlsendia.hospitalmanager.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class HistoryItem {
    private Long historyId;
    private Long customerId; //커스토머에서 가져오기
    private String customerName;
    private String customerPhone;
    private String registrationNumber;
    private String medicalItemName;
    private Double price;
    private String isSalaryName;
    private LocalDate dateCure;
    private LocalTime timeCure;
    private String isCalculate;
}