package com.wlsendia.hospitalmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class HistoryResponse {
    private Long historyId;
    private Long customerId;
    private String customerName;
    private String customerPhone;
    private String registrationNumber;
    private String address;
    private String memo;
    private String medicalItem;
    private LocalDate dateCure;
    private LocalTime timeCure;
    private Double productionPrice;
    private Double customerPrice;
    private Double insurancePrice;
    private Boolean isSalary;
    private Boolean isCalculate;
}
