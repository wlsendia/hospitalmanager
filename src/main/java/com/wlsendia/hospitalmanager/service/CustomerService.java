package com.wlsendia.hospitalmanager.service;

import com.wlsendia.hospitalmanager.entity.HospitalCustomer;
import com.wlsendia.hospitalmanager.model.CustomerRequest;
import com.wlsendia.hospitalmanager.repository.HospitalCustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

@Service
@RequiredArgsConstructor
public class CustomerService {

    private final HospitalCustomerRepository hospitalCustomerRepository;

    public void setCustomer(CustomerRequest request) {
        HospitalCustomer addData = new HospitalCustomer();
        addData.setCustomerName(request.getCustomerName());
        addData.setCustomerPhone(request.getCustomerPhone());
        addData.setRegistrationNumber(request.getRegistrationNumber());
        addData.setAddress(request.getAddress());
        addData.setMemo(request.getMemo());
        addData.setDateCreate(LocalDate.now());

        hospitalCustomerRepository.save(addData);
    }

    public HospitalCustomer getData(long id) {
        return hospitalCustomerRepository.findById(id).orElseThrow();
    }

}
