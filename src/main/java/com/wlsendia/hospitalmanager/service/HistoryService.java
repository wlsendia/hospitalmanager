package com.wlsendia.hospitalmanager.service;

import com.wlsendia.hospitalmanager.entity.ClinicHistory;
import com.wlsendia.hospitalmanager.entity.HospitalCustomer;
import com.wlsendia.hospitalmanager.model.HistoryResponse;
import com.wlsendia.hospitalmanager.model.HistoryInsuranceCorporationItem;
import com.wlsendia.hospitalmanager.model.HistoryItem;
import com.wlsendia.hospitalmanager.model.HistoryRequest;
import com.wlsendia.hospitalmanager.repository.ClinicHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final ClinicHistoryRepository clinicHistoryRepository;

    public void setHistory(HospitalCustomer hospitalCustomer, HistoryRequest historyRequest) {
        ClinicHistory addData = new ClinicHistory();
        addData.setHospitalCustomer(hospitalCustomer);
        addData.setMedicalItem(historyRequest.getMedicalItem());
        addData.setPrice(historyRequest.getIsSalary() ? historyRequest.getMedicalItem().getSalaryPrice() : historyRequest.getMedicalItem().getNonSalaryPrice());
        addData.setIsSalary(historyRequest.getIsSalary());
        addData.setDateCure(LocalDate.now());
        addData.setTimeCure(LocalTime.now());
        addData.setIsCalculate(false);

        clinicHistoryRepository.save(addData);
    }

    public List<HistoryItem> getHistoriesByDate(LocalDate searchDate) {
        List<HistoryItem> result = new LinkedList<>();

        List<ClinicHistory> originList = clinicHistoryRepository.findAllByDateCureOrderByIdDesc(searchDate);

        for (ClinicHistory item : originList) {
            HistoryItem addItem = new HistoryItem();
            addItem.setHistoryId(item.getId());
            addItem.setCustomerId(item.getHospitalCustomer().getId());
            addItem.setCustomerName(item.getHospitalCustomer().getCustomerName());
            addItem.setCustomerPhone(item.getHospitalCustomer().getCustomerPhone());
            addItem.setRegistrationNumber(item.getHospitalCustomer().getRegistrationNumber());
            addItem.setMedicalItemName(item.getMedicalItem().getName());
            addItem.setPrice(item.getPrice());
            addItem.setIsSalaryName(item.getIsSalary() ? "예" : "아니오");
            addItem.setDateCure(item.getDateCure());
            addItem.setTimeCure(item.getTimeCure());
            addItem.setIsCalculate(item.getIsCalculate() ? "예" : "아니오");
            result.add(addItem);
        }


        return result;
    }

    public List<HistoryInsuranceCorporationItem> getNeedBillCorporation(LocalDate searchDate) {
//        private String customerName;
//        private String customerPhone;
//        private String registrationNumber;
//        private String medicalItemName;
//        private Double medicalItemSalaryPrice;
//        private Double customerContributionPrice;
//        private Double billingAmount;
//        private LocalDate dateCure;
        List<HistoryInsuranceCorporationItem> result = new LinkedList<>();

        List<ClinicHistory> notOriginList = clinicHistoryRepository.
                findAllByIsSalaryAndDateCureAndIsCalculateOrderById(true, searchDate, true);

        for (ClinicHistory list : notOriginList) {
            HistoryInsuranceCorporationItem addItem = new HistoryInsuranceCorporationItem();
            addItem.setCustomerName(list.getHospitalCustomer().getCustomerName());
            addItem.setCustomerPhone(list.getHospitalCustomer().getCustomerPhone());
            addItem.setRegistrationNumber(list.getHospitalCustomer().getRegistrationNumber());
            addItem.setMedicalItemName(list.getMedicalItem().getName());
            addItem.setMedicalItemSalaryPrice(list.getMedicalItem().getNonSalaryPrice());
            addItem.setCustomerContributionPrice(list.getPrice());
            addItem.setBillingAmount(addItem.getMedicalItemSalaryPrice() - addItem.getCustomerContributionPrice());
            addItem.setDateCure(searchDate);//맞나?

            result.add(addItem);
        }

        return result;

    }

    public HistoryResponse getHistory(long id){
        ClinicHistory origin = clinicHistoryRepository.findAllById(id);

        HistoryResponse result = new HistoryResponse();

        result.setHistoryId(id);
        result.setCustomerId(origin.getId());
        result.setCustomerName(origin.getHospitalCustomer().getCustomerName());
        result.setCustomerPhone(origin.getHospitalCustomer().getCustomerPhone());
        result.setRegistrationNumber(origin.getHospitalCustomer().getRegistrationNumber());
        result.setAddress(origin.getHospitalCustomer().getAddress());
        result.setMemo(origin.getHospitalCustomer().getMemo());
        result.setMedicalItem(origin.getMedicalItem().getName());
        result.setDateCure(origin.getDateCure());
        result.setTimeCure(origin.getTimeCure());
        result.setProductionPrice(origin.getMedicalItem().getNonSalaryPrice());
        result.setCustomerPrice(origin.getPrice());//
        result.setInsurancePrice(origin.getMedicalItem().getNonSalaryPrice()-origin.getPrice());
        result.setIsSalary(origin.getIsSalary());
        result.setIsCalculate(origin.getIsCalculate());


        return result;


    }

}
